DESTDIR=/

all:

check:

install:
	mkdir -p -m 755 $(DESTDIR)usr/bin
	mkdir -p -m 755 $(DESTDIR)usr/share/pixmaps
	mkdir -p -m 755 $(DESTDIR)usr/share/applications
	install -m 755 base/unfatarians-studio $(DESTDIR)usr/bin/
	install -m 644 base/unfatarians-studio.png $(DESTDIR)usr/share/pixmaps/
	install -m 644 base/unfatarians-studio.desktop $(DESTDIR)usr/share/applications/

uninstall:
	-sudo pacman -R unfatarians-studio
	rm -f $(DESTDIR)usr/bin/unfatarians-studio
	rm -f $(DESTDIR)usr/share/pixmaps/unfatarians-studio.png
	rm -f $(DESTDIR)usr/share/applications/unfatarians-studio.desktop
