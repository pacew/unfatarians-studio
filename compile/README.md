# Clean compile environment

We isolate the compile environment for the AUR plugins from the host
OS using a separate docker container.  The big ideas are:

* create an image containing the compiler

* create a container running the image in a background process and
with a regular host directory mounted on /aur

* connect to the container and download AUR packages into /aur,
compile them, leaving the compiled .pkg.tar.zst files on the host file
system

* pakages are also installed inside the container as they are built,
which allows each AUR package to depend on previously compiled AUR
packages 

There's a compile-all script that compiles all desired AUR packages
sequentially.  Ideally this will work from scratch without
intervention.  But if something does go wrong, the container running
in the background holds the intermediate state, so you can figure out
what went wrong and likely patch it up and continue.  If you do
intervene this way, you'll have to use your judgement about whether
the resulting .pkg.tar.vst files are suitable for public release, or
if you should fix the underlying error (maybe you filled in a missing
dependency), clear all the caches, and try again from the top.

In detail:

* make clean

Cleans most files and processes

* ./dist-clean

Clears the rest of the caches, specifically the downloaded AUR
packages in ~/.cache/unfatarians-studio/aur

* make

Builds the docker image

* ./start

Launches the container as a background process

* ./stop

Kill the container that's running in the background 

* ./attach

Get a shell in the container. Type control-q to detach from the
container and leave it running in the background.

* ./compile AUR-PACKAGE

Compile a single AUR package in the container.  This includes
initially cloning the PKGBUILD file and installing any standard system
package that the AUR package depends on.  If the compilation is
successful, the package is also installed within the container

* ./compile-helper

The compile script causes ./compile-helper to run inside the container
to do the actual work of compiling

* ./compile-all

Compiles a list of AUR packages inside the container.  This list of
packages is a literal inside this file.

