import subprocess

bin_packages = [
    'binutils',
    'pipewire',
    'pipewire-jack',
    'ardour',
    'carla',
    'mesa-utils',
    'libcanberra',

    'ttf-dejavu',  # needed for odin2
    'ttf-liberation',  # needed for odin2
    

    # to add plugins from the arch community or extra repositories,
    # just put their names here
    'calf',
    'x42-plugins',
    'gmsynth.lv2',
    'guitarix',
    'distrho-ports',
    'dragonfly-reverb',
    'geonkick',
    'lsp-plugins',
    'zam-plugins',
    'gxplugins.lv2',
    'wolf-shaper',
    'wolf-spectrum',
    'dpf-plugins',

    # debugging support
    'jalv',
    'lilv',
    'gdb',
]

bin_packages_new = [
]

# to add aur packages:
#  run utils/aur-add NAME
#  run utils/aur-build
#  add the name here
aur_packages = [
    'aether.lv2',
    'airwindows-git',

    # this is, alas, a normal error for odin2 when running ./build
    #   installing odin2-synthesizer...
    #   error: command failed to execute correctly
    # if you run with pacman --debug -U ... it will show the script
    # that fails is .INSTALL, and looking into the odin2 package,
    # it is clear that this is not important to us
    'odin2-synthesizer',
]

def make_dockerfile():
    dfile = 'FROM archlinux\n'

    dfile += 'RUN echo 6\n'  # increment to invalidate docker layer cache

    dfile += 'COPY mirrorlist /etc/pacman.d/mirrorlist\n'
    dfile += 'RUN pacman --noconfirm -Syu\n'
    dfile += 'RUN sed --in-place "/NoExtract.*locale/d" /etc/pacman.conf\n'
    dfile += 'RUN pacman --noconfirm -S '
    dfile += ' '.join(bin_packages)
    dfile += '\n\n'

    # write the checksum of the tar file into the Dockerfile, so that
    # if the tar file changes, docker will rebuild this layer
    result = subprocess.run('cksum aur.tar', 
                            shell=True, text=True, capture_output=True)
    cksum = result.stdout.split()[0]
    dfile += f'RUN echo {cksum}\n'
    dfile += 'COPY aur.tar /aur/\n'

    dfile += 'RUN tar -C /aur -xf /aur/aur.tar\n'

    dfile += 'RUN cd /aur; pacman --noconfirm -U /aur/*.pkg.tar.zst\n'

    if len(bin_packages_new) > 0:
        dfile += 'RUN pacman --noconfirm -S '
        dfile += ' '.join(bin_packages_new)
        dfile += '\n'

    dfile += 'CMD bash\n'

    with open('Dockerfile', 'w') as outf:
        outf.write(dfile)

make_dockerfile()
