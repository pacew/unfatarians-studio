pace's experiments with caching aur package builds

usage:

Only tested with archlinux as the host OS.

Get pipewire working on your host first, then:

```
git clone git@codeberg.org:pacew/unfatarians-studio.git
cd unfatarians-studio
follow instructions in compile/README.md to get the compile container running
./utils/aur-build
cd base
make
./build
./run
```

Your home directory inside the container will be mapped to $HOME/u-studio
