# unfatarians-studio

An easy to install package containing ardour and a rich set of plugins.  This is a great base for a collaborative project, since everyone will run the same software.

Currently in alpha test ... please experiment, but don't depend on it for important work.

unfatarians-studio is intended to be used on a workstation where the user is also the administrator.

* [Documentation](/pacew/unfatarians-studio/wiki)
* [Installation and running](/pacew/unfatarians-studio/wiki/installation)
* [Roadmap](/pacew/unfatarians-studio/wiki/Roadmap)
* [Issue tracker](/pacew/unfatarians-studio/issues)


